# kata-starters

Personal repository to store code starters for katas.

There are 2 distinct folder to chose from:

- **technical-starters:** contains ready to use empty projects for different languages 
- **kata-starters:**  contains actuel katas with instructions, but no coded yet
